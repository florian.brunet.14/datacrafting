from django.contrib import admin
from .models import Path, Stage, User, Trip, Continent, Country, StageImage, PathImage, StageArticle, PathArticle
from leaflet.admin import LeafletGeoAdmin

admin.site.register(PathArticle)
admin.site.register(StageArticle)
admin.site.register(PathImage)
admin.site.register(StageImage)
admin.site.register(Country, LeafletGeoAdmin)
admin.site.register(Continent, LeafletGeoAdmin)
admin.site.register(Trip)
admin.site.register(Path, LeafletGeoAdmin)
admin.site.register(Stage, LeafletGeoAdmin)
admin.site.register(User)
