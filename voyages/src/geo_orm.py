def extract_coords_from_path(path_queryset):

    path_list = list(path_queryset.values())
    path_list_formatted = []
    for path in path_list:
        path_formatted = {}
        for path_field in path:
            if path_field == 'geom':
                path_formatted[path_field] = path[path_field].json
            else:
                path_formatted[path_field] = path[path_field]
        path_list_formatted.append(path_formatted)

    return path_list_formatted
