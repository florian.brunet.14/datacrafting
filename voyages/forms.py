from django import forms
from .models import Stage, Trip, StageArticle, User
import datetime
from leaflet.forms.widgets import LeafletWidget


class ArticleForm(forms.ModelForm):
    class Meta:
        model = StageArticle
        fields = '__all__'


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = '__all__'
        widgets = {
            'pwd': forms.PasswordInput(),
        }


class StageForm(forms.ModelForm):
    day_start = forms.DateField(widget=forms.SelectDateWidget(years=range(1990, datetime.datetime.now().year + 1)))
    day_end = forms.DateField(widget=forms.SelectDateWidget(years=range(1990, datetime.datetime.now().year + 1)))

    class Meta:
        model = Stage
        fields = '__all__'
        widgets = {'geom': LeafletWidget()}

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(StageForm, self).__init__(*args, **kwargs)


class TripFormAj(forms.Form):

    def __init__(self, *args, **kwargs):
        pseudo = kwargs.pop('pseudo')
        super(TripFormAj, self).__init__(*args, **kwargs)
        choix = [(t.id, t.name) for t in Trip.objects.filter(user__pseudo=pseudo).order_by('-time_end')]
        self.fields["voyageaj"] = forms.ChoiceField(choices=choix, label='', widget=forms.Select(attrs={'class': 'selectpicker', 'data-style': 'btn-primary'}))
