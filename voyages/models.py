from __future__ import unicode_literals
from django.contrib.gis.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class User(models.Model):
    pseudo = models.CharField(max_length=32)
    surname = models.CharField(max_length=32, blank=True, null=True)
    name = models.CharField(max_length=32, blank=True, null=True)
    email = models.CharField(max_length=64)
    address = models.CharField(max_length=128, blank=True, null=True)
    ville = models.CharField(max_length=64, blank=True, null=True)
    cp = models.CharField(max_length=5, blank=True, null=True)
    pwd = models.CharField(max_length=16)
    created_at = models.DateTimeField(blank=True, auto_now_add=True)
    
    def __str__(self):
        return self.pseudo + " (" + self.surname + " " + self.name + ")"


class Trip(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE, default=0)
    name = models.CharField(max_length=128, blank=False, null=False)
    time_start = models.DateField(blank=True, null=True)
    time_end = models.DateField(blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    descr = models.TextField(blank=True, null=True)
    note = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name


class Path(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE, default=0)
    trip = models.ForeignKey('Trip', on_delete=models.CASCADE, default=0)
    step_start = models.ForeignKey('Stage', on_delete=models.PROTECT, related_name='starting_stage', default=0)
    step_end = models.ForeignKey('Stage', on_delete=models.PROTECT, related_name='ending_stage', default=0)
    time_start = models.DateTimeField(blank=True, null=True)
    time_end = models.DateTimeField(blank=True, null=True)
    transport = models.CharField(max_length=2, blank=True, null=True, help_text='bi : bike, bo : boat, bu : bus, c : car, t : train, p : plane, s : stop, w : walk')
    text = models.TextField(blank=True, null=True)
    geom = models.MultiLineStringField(dim=2, blank=True, null=True)

    def __str__(self):
        return "De " + self.step_start.town + " à " + self.step_end.town


class PathArticle(models.Model):
    path = models.ForeignKey('Path', on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=128)
    text = models.TextField(blank=True, null=True)
    published_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.title


class Stage(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE, null=True)
    trip = models.ForeignKey('Trip', on_delete=models.CASCADE, null=True)
    country = models.ForeignKey('Country', on_delete=models.PROTECT, null=True, blank=True)
    address = models.CharField(max_length=128, blank=True, null=True)
    zip = models.CharField(max_length=8, blank=True, null=True)
    town = models.CharField(max_length=64, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    notes = models.IntegerField(blank=True, null=True, validators=[MaxValueValidator(5), MinValueValidator(0)])
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True)
    day_start = models.DateField(blank=True, null=True)
    day_end = models.DateField(blank=True, null=True)
    on_path = models.BooleanField(default=True)
    geom = models.PointField(dim=2, blank=True, null=True, srid=4326)

    def __str__(self):
        return self.town


class StageArticle(models.Model):
    stage = models.ForeignKey('Stage', on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=128)
    text = models.TextField(blank=True, null=True)
    published_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.title


class Country(models.Model):
    code_iso = models.CharField(max_length=2, blank=True, null=True)
    label_local = models.CharField(max_length=128)
    label_en = models.CharField(max_length=128, blank=True, null=True)
    label_fr = models.CharField(max_length=128, blank=True, null=True)
    continent = models.ForeignKey('Continent', on_delete=models.PROTECT, blank=True, null=True)
    geom = models.MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.label_local


class Continent(models.Model):
    label = models.CharField(max_length=16)
    geom = models.MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.label


def user_directory_stage_img(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/trip_{1}/stage_{2}/{3}'.format(instance.stage.user.id, instance.stage.trip.name, instance.stage.town, filename)


class StageImage(models.Model):
    stage = models.ForeignKey('Stage', on_delete=models.CASCADE)
    stage_article = models.ForeignKey('StageArticle', on_delete=models.SET_NULL, null=True, blank=True)
    img = models.ImageField(upload_to=user_directory_stage_img)


def user_directory_path_img(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/trip_{1}/stage_{2}_to_stage_{3}/{4}'.format(instance.path.user.id, instance.path.trip.name, instance.path.step_start.town, instance.path.step_end.town, filename)


class PathImage(models.Model):
    path = models.ForeignKey('Path', on_delete=models.CASCADE)
    img = models.ImageField(upload_to=user_directory_path_img)
