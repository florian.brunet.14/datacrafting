from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .models import Stage, Trip, StageImage, Path, StageArticle
from django.views import generic
from .forms import StageForm, ArticleForm, UserForm, TripFormAj
from .src.geo_orm import extract_coords_from_path


class IndexView(generic.ListView):
    template_name = 'voyages/stages_list.html'
    context_object_name = 'latest_stages'
    
    def get_queryset(self):
        """Return the last five stages"""
        return Stage.objects.order_by('-day_start')[:5]


class DetailView(generic.DetailView):
    model = Stage
    template_name = 'voyages/stage_details.html'


##############################
# Etapes d'un voyage #########
##############################


def extract_trip_stages(request):
    trip_id = int(request.GET['id_trip'])
    data = {
        str(i.pk) + '_' + i.town: {
            "text": i.text,
            "on_path": i.on_path,
            "coords": [i.geom.y, i.geom.x],
            "photos": list(StageImage.objects.filter(stage__pk=i.pk).values()),
            "path": extract_coords_from_path(Path.objects.filter(step_start__pk=i.pk)),
            "article": list(StageArticle.objects.filter(stage__pk=i.pk).order_by('published_at').values())
        }
        for i in Stage.objects.filter(trip=trip_id).order_by('day_start', 'day_end')
        if i.geom is not None}

    return JsonResponse(data)

############################
# Home d'un user ###########
############################


def user_home(request, pseudo):

    trip_list = Trip.objects.filter(user__pseudo=pseudo).order_by('-time_end')

    # Construire le formulaire, soit avec les données postées,
    # soit vide si l'utilisateur accède pour la première fois
    # à la page.
    # form = TripForm2(request.GET or None)
    form_aj = TripFormAj(pseudo=pseudo)

    trip_selected = None
    if 'voyage' in request.GET:
        trip_selected = Trip.objects.filter(id=request.GET['voyage'])
        return HttpResponse(trip_selected)

    return render(request, 'voyages/user_home.html', {'trips': trip_list, 'form_aj': form_aj})


####################
# Affichage articles
####################

def print_stage_article(request, pseudo, trip, stage):

    first_article = list(StageArticle.objects.filter(stage__pk=stage).values())[0]
    stage = Stage.objects.get(pk=first_article['stage_id'])

    return render(request, 'voyages/display_article.html', {"article": first_article, "stage": stage})


########################################
############## formulaires #############
########################################


def stage(request):
    # Construire le formulaire, soit avec les données postées,
    # soit vide si l'utilisateur accède pour la première fois
    # à la page.
    form = StageForm(request.POST or None)
    # Nous vérifions que les données envoyées sont valides
    # Cette méthode renvoie False s'il n'y a pas de données
    # dans le formulaire ou qu'il contient des erreurs.
    if form.is_valid():
        # Ici nous pouvons traiter les données du formulaire
        form.save()
        envoi = True

    # Quoiqu'il arrive, on affiche la page du formulaire.
    return render(request, 'voyages/stage_form.html', locals())


def article(request):
    # Construire le formulaire, soit avec les données postées,
    # soit vide si l'utilisateur accède pour la première fois
    # à la page.
    form = ArticleForm(request.POST or None)
    # Nous vérifions que les données envoyées sont valides
    # Cette méthode renvoie False s'il n'y a pas de données
    # dans le formulaire ou qu'il contient des erreurs.
    if form.is_valid():
        # Ici nous pouvons traiter les données du formulaire
        form.save()
        envoi = True

    # Quoiqu'il arrive, on affiche la page du formulaire.
    return render(request, 'voyages/article_form.html', locals())


def user(request):
    # Construire le formulaire, soit avec les données postées,
    # soit vide si l'utilisateur accède pour la première fois
    # à la page.
    form = UserForm(request.POST or None)
    # Nous vérifions que les données envoyées sont valides
    # Cette méthode renvoie False s'il n'y a pas de données
    # dans le formulaire ou qu'il contient des erreurs.
    if form.is_valid():
        # Ici nous pouvons traiter les données du formulaire
        form.save()
        envoi = True

    # Quoiqu'il arrive, on affiche la page du formulaire.
    return render(request, 'voyages/user_form.html', locals())
