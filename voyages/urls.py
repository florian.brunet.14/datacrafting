from django.urls import path
from . import views

urlpatterns = [
    path('user/<str:pseudo>/', views.user_home),
    path('user/<str:pseudo>/<str:trip>/<str:stage>/', views.print_stage_article),
    path('ajax/add_poi/', views.extract_trip_stages, name='extract_trip_stages'),
    path('last_stages/', views.IndexView.as_view(), name='last_stages'),
    path('stage/<int:pk>/', views.DetailView.as_view(), name='stage_detail'),
]
