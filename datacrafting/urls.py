"""dataflo_django URL Configuration

The `pathpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/paths/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to pathpatterns:  path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to pathpatterns:  path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.paths import path, include
    2. Add a URL to pathpatterns:  path(r'^blog/', include('blog.paths'))
"""

from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path
from django.contrib import admin
from . import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('mysite.urls')),
    path('athle/', include('ffa.urls')),
    path('voyages/', include('voyages.urls')),
]


urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
