// Slideshow functions
var slideIndex = 1;

// change to the next / prev image
function plusSlides(n, arr_photo) {
  console.log("plus");
  showSlides(slideIndex += n, arr_photo);
  console.log("plus ok");
}

function currentSlide(n, arr_photo) {
  console.log("array : " + Array.isArray(arr_photo));
  console.log("array len : " + arr_photo.length);
  console.log("photo list :" + arr_photo);
  console.log("current slide photo : " + arr_photo[n-1]);
  setTimeout(function() { showSlides(slideIndex = n, arr_photo); }, 300);
}


// display only target image
function showSlides(n, arr_photo) {
  console.log(n);
  console.log("arr_photo : " + arr_photo);
  var slideshow = document.getElementsByClassName("slideshow-container")[0];
  var i, id_photo, img_id, div_slides, append_as_last, img_name, slide_node;
  var slides = document.getElementsByClassName("mySlides");
  var nb_img = slides.length, total_img = arr_photo.length;
  var last_img = nb_img + 2;
  var init_slides = "<div class=mySlides fade>";
  var init_img = "<img id='img_";
  var end_img = "'>";
  var init_img_counter = "<span class='numbertext'>";
  var end_span = "</span>";
  var left_arrow = "<div class='img_switch'><a class=prev>&#10094;</a>";
  var right_arrow = "<a class=next>&#10095;</a></div>";
  var end_div = "</div>";

  if (nb_img === 0) {
    console.log("Pas d'images présentes sur ce point");
    for(i = 0; i < last_img; i++){
      id_photo = i + 1;
      img_str = "";
      img_str += init_slides + init_img + id_photo + "' src='" + arr_photo[i] + end_img
      img_str += left_arrow + init_img_counter + id_photo + " / " + total_img + end_span + right_arrow + end_div

      div_slides = document.createElement('div');
      div_slides.innerHTML = img_str;
      slideshow.appendChild(div_slides.firstChild);
      console.log("ajout image " + id_photo + " / " + total_img);
      console.log("nombre d'images objet slides = " + slides.length);
      console.log("nombre d'images = " + document.getElementsByClassName("mySlides").length);
      img_name = "img_" + id_photo;
      slide_node = document.getElementById(img_name).parentNode;
      if (arr_photo.length > 1) {
          slide_node.lastChild.addEventListener('click', function() {
            plusSlides(1,arr_photo);
          }, false);
          slide_node.lastChild.previousSibling.addEventListener('click', function() {
            plusSlides(-1,arr_photo);
          }, false);
      }
    }
  } else {
    var present_img = []; // list all id of photos already present
    for (i = 0; i < nb_img; i++){
      present_img.push(slides[i].childNodes[0].id);
    }
    console.log("photos deja presentes : " + present_img);
    for (i = n; Math.min((n + 2), total_img) >= i; i++) {
      console.log(i + " / " + Math.min((n + 2), total_img));
      if (i <= total_img) {
       img_id = "img_" + i;
       console.log("image a checker : " + img_id);
       console.log("index image : " + present_img.indexOf(img_id));
       if (present_img.indexOf(img_id) === -1) { // if img not present
         console.log(img_id + " non presentes");
         img_str = "";
         img_str += init_slides + init_img + i + "' src='" + arr_photo[i-1] + end_img
         img_str += left_arrow + init_img_counter + i + " / " + total_img + end_span + right_arrow + end_div
         div_slides = document.createElement('div');
         div_slides.innerHTML = img_str;
         slides = document.getElementsByClassName("mySlides");
         var nb_slides = slides.length;
         append_as_last = true;
         for (var j = 0; j < nb_slides; j++) {
           console.log("j = " + j);
           console.log(slides);
           console.log("id = " + parseInt(slides[j].childNodes[0].id.substring(4)));
           console.log("i = " + i);
           if(parseInt(slides[j].childNodes[0].id.substring(4)) > i) {
             console.log("inserer avant " + j);
             slides[j].parentNode.insertBefore(div_slides.firstChild, slides[j]);
             console.log("insertion avant ok!");
             append_as_last = false;
             break;
            }
          }
          if (append_as_last) {
            slideshow.appendChild(div_slides.firstChild);
            console.log("insertion a la fin");
          }
          img_name = "img_" + i;
          console.log(img_name);
          slide_node = document.getElementById(img_name).parentNode;
          slide_node.lastChild.addEventListener('click', function() {
            plusSlides(1,arr_photo);}, false);
          slide_node.lastChild.previousSibling.addEventListener('click', function() {
            plusSlides(-1,arr_photo);}, false);
        }
      }
    }
  }
  var final_img = []; // list all id of photos already present
  nb_img = slides.length;
  for (i = 0; i < nb_img; i++){
    final_img.push(slides[i].childNodes[0].id);
  }
  console.log("photos presentes a la fin : " + final_img);
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "block";
}