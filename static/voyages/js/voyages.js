function onClickMarker(e, data) {
    console.log(e);
   var popup = e.target.getPopup();
   var content = popup.getContent();
   console.log(content);
   id_stage_splitted = e.target._icon.id.split('_');
   name_stage = id_stage_splitted[1] + '_' + id_stage_splitted[2];
   photos_path = data[name_stage].photos.map(x => "/media/" + x.img);
   console.log(photos_path);
   if (photos_path.length > 0) {
        if (content.includes("slideshow-container") == false) {
            popup.setContent("<div class='slideshow-container'></div>".concat(content));
        }
       currentSlide(1, photos_path);
   }
}


var maj_map = function (e) {
    trip_name = $("#id_voyageaj option:selected").text().replace(/\s+/g, '-').toLowerCase();
    console.log($("#id_voyageaj option:selected").val() + " : " + trip_name);

    $.ajax({
        url: "/voyages/ajax/add_poi",
        data: {
            'id_trip': $("#id_voyageaj option:selected").val()
        },
        dataType: 'json',
        success: function (data) {
            console.log(data);

            nb_steps = Object.keys(data).length;
            nb_steps_on_path = Object.entries(data).filter(([k,v]) => v.on_path).length;
            present_steps = $("." + trip_name).length;
            if (nb_steps > present_steps) {
                if (present_steps > 0) {
                    $("." + trip_name).remove();
                }
                all_coords = {'west': -180, 'east': 180, 'south': -90, 'north': 90}
                all_bounding_view[trip_name] = [];
                i=0;
                for (key in data) {
                    position = data[key].coords;
                    all_bounding_view[trip_name].push(position);
                    console.log(position);
                    if (data[key].on_path) {
                        if (i==0) {
                            jobMarkerIcon = L.AwesomeMarkersNoNumber.icon({
                                icon: 'play',
                                prefix: 'glyphicon'
                            });
                        } else if (i==(nb_steps_on_path-1)) {
                            jobMarkerIcon = L.AwesomeMarkersNoNumber.icon({
                                icon: 'flag',
                                prefix: 'glyphicon'
                            });
                        } else {
                            jobMarkerIcon = L.AwesomeMarkers.icon({
                                icon: '',
                                markerColor: 'darkblue',
                                prefix: 'fa',
                                html: i
                            });
                        }
                        i++;
                    } else {
                        jobMarkerIcon = L.AwesomeMarkersNoNumber.icon({
                                icon: 'eye-open',
                                prefix: 'glyphicon',
                                markerColor: 'green'
                            });
                    }

                    marker = L.marker(position, {icon: jobMarkerIcon});
                    marker.addTo(map);
                    photo_list = data[key].photos;

                    articles = data[key].article;
                    if (articles.length > 0) {
                        console.log(data[key]);
                        extract_length = 200;
                        article_title = data[key].article[0].title;
                        article_extract = data[key].article[0].text.substring(0, extract_length);
                        html_popup = "<h4>" + article_title + "</h4>"
                        html_popup += "<span>" + article_extract + "</span>"
                        if (data[key].article[0].text.length > extract_length) {
                            url = "/voyages/user/" + window.location.pathname.split("/")[3]
                            url += "/" + key + "/" + data[key].article[0].id + "/"
                            html_popup += " [...]<br><a href='" + url + "'>Lire la suite</a>"
                        }
                        marker.bindPopup(html_popup, {maxWidth: (window.screen.width / 3.5)});
                    } else {
                        marker.bindPopup("Aucun article");
                    }
                    marker.on('click', function(e) {
                        onClickMarker(e, data);
                    });
                    $(marker._icon).addClass(trip_name + "-marker");
                    $(marker._icon).attr('id', 'stage_'.concat(key));

                    if (data[key].path.length > 0) {
                        L.geoJSON(JSON.parse(data[key].path[0].geom)).addTo(map);
                    }
                }
            }
            map.fitBounds(all_bounding_view[trip_name], { padding: [ 50, 50 ], maxZoom: 12 });
        }
    });

    $(".leaflet-marker-icon").hide();
    $("." + trip_name).show();
}



