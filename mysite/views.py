from django.shortcuts import render
from django.http import HttpResponse


def hello(request, pseudo = ""):
    page = "<h1>Dataflo</h1>\n"
    page += "<h2>Un site de geek de flobrunette</h2>\n"
    if pseudo != "":
        page += "<h3>Bienvenue Mr {}".format(pseudo)
    #return HttpResponse(page)
    return render(request, "mysite/main.html")


def homepage(request):
    return render(request, "mysite/homepage.html")